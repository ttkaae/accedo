"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DragScrollDirective = (function () {
    function DragScrollDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.isPressed = false;
        this.isScrolling = false;
        this.downX = 0;
        this.downY = 0;
        this.displayType = 'block';
        this.onMouseMoveHandler = this.onMouseMove.bind(this);
        this.onMouseDownHandler = this.onMouseDown.bind(this);
        this.onScrollHandler = this.onScroll.bind(this);
        this.onMouseUpHandler = this.onMouseUp.bind(this);
        this.currIndex = 0;
        this.isAnimating = false;
        this.scrollReachesRightEnd = false;
        this.prevChildrenLength = 0;
        this.childrenArr = [];
        this.reachesLeftBound = new core_1.EventEmitter();
        this.reachesRightBound = new core_1.EventEmitter();
        this.scrollbarWidth = this.getScrollbarWidth() + "px";
        el.nativeElement.style.overflow = 'auto';
        el.nativeElement.style.whiteSpace = 'noWrap';
        this.mouseDownListener = renderer.listen(el.nativeElement, 'mousedown', this.onMouseDownHandler);
        this.scrollListener = renderer.listen(el.nativeElement, 'scroll', this.onScrollHandler);
        this.mouseMoveListener = renderer.listen('document', 'mousemove', this.onMouseMoveHandler);
        this.mouseUpListener = renderer.listen('document', 'mouseup', this.onMouseUpHandler);
    }
    DragScrollDirective.prototype.disableScroll = function (axis) {
        this.el.nativeElement.style["overflow-" + axis] = 'hidden';
    };
    DragScrollDirective.prototype.enableScroll = function (axis) {
        this.el.nativeElement.style["overflow-" + axis] = 'auto';
    };
    DragScrollDirective.prototype.hideScrollbar = function () {
        if (this.el.nativeElement.style.display !== 'none' && !this.wrapper) {
            this.parentNode = this.el.nativeElement.parentNode;
            this.wrapper = this.el.nativeElement.cloneNode(true);
            if (this.wrapper !== null) {
                while (this.wrapper.hasChildNodes()) {
                    if (this.wrapper.lastChild !== null) {
                        this.wrapper.removeChild(this.wrapper.lastChild);
                    }
                }
                this.wrapper.style.overflow = 'hidden';
                this.el.nativeElement.style.width = "calc(100% + " + this.scrollbarWidth + ")";
                this.el.nativeElement.style.height = "calc(100% + " + this.scrollbarWidth + ")";
                this.parentNode.replaceChild(this.wrapper, this.el.nativeElement);
                this.wrapper.appendChild(this.el.nativeElement);
            }
        }
    };
    DragScrollDirective.prototype.showScrollbar = function () {
        if (this.wrapper) {
            this.el.nativeElement.style.width = this.elWidth;
            this.el.nativeElement.style.height = this.elHeight;
            this.parentNode.removeChild(this.wrapper);
            this.parentNode.appendChild(this.el.nativeElement);
            this.wrapper = null;
        }
    };
    DragScrollDirective.prototype.checkScrollbar = function () {
        if (this.el.nativeElement.scrollWidth <= this.el.nativeElement.clientWidth) {
            this.el.nativeElement.style.height = '100%';
        }
        else {
            this.el.nativeElement.style.height = "calc(100% + " + this.scrollbarWidth + ")";
        }
        if (this.el.nativeElement.scrollHeight <= this.el.nativeElement.clientHeight) {
            this.el.nativeElement.style.width = '100%';
        }
        else {
            this.el.nativeElement.style.width = "calc(100% + " + this.scrollbarWidth + ")";
        }
    };
    DragScrollDirective.prototype.setScrollBar = function () {
        if (this.scrollbarHidden) {
            this.hideScrollbar();
        }
        else {
            this.showScrollbar();
        }
    };
    DragScrollDirective.prototype.getScrollbarWidth = function () {
        var widthNoScroll = 0;
        var widthWithScroll = 0;
        var outer = document.createElement('div');
        if (outer !== null) {
            outer.style.visibility = 'hidden';
            outer.style.width = '100px';
            outer.style.msOverflowStyle = 'scrollbar';
            document.body.appendChild(outer);
            widthNoScroll = outer.offsetWidth;
            outer.style.overflow = 'scroll';
            var inner = document.createElement('div');
            inner.style.width = '100%';
            outer.appendChild(inner);
            widthWithScroll = inner.offsetWidth;
            if (outer.parentNode !== null) {
                outer.parentNode.removeChild(outer);
            }
        }
        return widthNoScroll - widthWithScroll || 20;
    };
    DragScrollDirective.prototype.scrollTo = function (element, to, duration) {
        var self = this;
        self.isAnimating = true;
        var start = element.scrollLeft, change = to - start, increment = 20;
        var currentTime = 0;
        var easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) {
                return c / 2 * t * t + b;
            }
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };
        var animateScroll = function () {
            currentTime += increment;
            element.scrollLeft = easeInOutQuad(currentTime, start, change, duration);
            if (currentTime < duration) {
                self.scrollToTimer = window.setTimeout(animateScroll, increment);
            }
            else {
                setTimeout(function () {
                    self.isAnimating = false;
                }, increment);
            }
        };
        animateScroll();
    };
    DragScrollDirective.prototype.locateCurrentIndex = function (snap) {
        var _this = this;
        var ele = this.el.nativeElement;
        this.currentChildWidth(function (currentClildWidth, nextChildrenWidth, childrenWidth, idx, stop) {
            if (ele.scrollLeft >= childrenWidth &&
                ele.scrollLeft <= nextChildrenWidth) {
                if (nextChildrenWidth - ele.scrollLeft > currentClildWidth / 2 && !_this.scrollReachesRightEnd) {
                    _this.currIndex = idx;
                    if (snap) {
                        _this.scrollTo(ele, childrenWidth, 500);
                    }
                }
                else {
                    _this.currIndex = idx + 1;
                    if (snap) {
                        _this.scrollTo(ele, childrenWidth + currentClildWidth, 500);
                    }
                }
                stop();
            }
        });
    };
    DragScrollDirective.prototype.currentChildWidth = function (cb) {
        var childrenWidth = 0;
        var shouldBreak = false;
        var breakFunc = function () {
            shouldBreak = true;
        };
        for (var i = 0; i < this.childrenArr.length; i++) {
            if (i === this.childrenArr.length - 1) {
                this.currIndex = this.childrenArr.length;
                break;
            }
            if (shouldBreak) {
                break;
            }
            var nextChildrenWidth = childrenWidth + this.childrenArr[i + 1].clientWidth;
            var currentClildWidth = this.childrenArr[i].clientWidth;
            cb(currentClildWidth, nextChildrenWidth, childrenWidth, i, breakFunc);
            childrenWidth += this.childrenArr[i].clientWidth;
        }
    };
    DragScrollDirective.prototype.toChildrenLocation = function () {
        var to = 0;
        for (var i = 0; i < this.currIndex; i++) {
            to += this.childrenArr[i].clientWidth;
        }
        return to;
    };
    DragScrollDirective.prototype.resetScrollLocation = function () {
        var ele = this.el.nativeElement;
        this.scrollTo(ele, 0, 0);
        this.currIndex = 0;
    };
    DragScrollDirective.prototype.markElDimension = function () {
        if (this.wrapper) {
            this.elWidth = this.wrapper.style.width;
            this.elHeight = this.wrapper.style.height;
        }
        else {
            this.elWidth = this.el.nativeElement.style.width;
            this.elHeight = this.el.nativeElement.style.height;
        }
    };
    Object.defineProperty(DragScrollDirective.prototype, "scrollbarHidden", {
        get: function () { return this._scrollbarHidden; },
        set: function (value) { this._scrollbarHidden = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DragScrollDirective.prototype, "disabled", {
        get: function () { return this._disabled; },
        set: function (value) { this._disabled = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DragScrollDirective.prototype, "xDisabled", {
        get: function () { return this._xDisabled; },
        set: function (value) { this._xDisabled = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DragScrollDirective.prototype, "yDisabled", {
        get: function () { return this._yDisabled; },
        set: function (value) { this._yDisabled = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DragScrollDirective.prototype, "dragDisabled", {
        get: function () { return this._dragDisabled; },
        set: function (value) { this._dragDisabled = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DragScrollDirective.prototype, "snapDisabled", {
        get: function () { return this._snapDisabled; },
        set: function (value) { this._snapDisabled = value; },
        enumerable: true,
        configurable: true
    });
    DragScrollDirective.prototype.onResize = function () {
        this.markElDimension();
        this.resetScrollLocation();
        this.checkNavStatus();
    };
    DragScrollDirective.prototype.attach = function (_a) {
        var disabled = _a.disabled, scrollbarHidden = _a.scrollbarHidden, yDisabled = _a.yDisabled, xDisabled = _a.xDisabled;
        this.disabled = disabled;
        this.scrollbarHidden = scrollbarHidden;
        this.yDisabled = yDisabled;
        this.xDisabled = xDisabled;
        this.ngOnChanges();
    };
    DragScrollDirective.prototype.ngOnChanges = function () {
        this.setScrollBar();
        this.resetScrollLocation();
        if (this.xDisabled || this.disabled) {
            this.disableScroll('x');
        }
        else {
            this.enableScroll('x');
        }
        if (this.yDisabled || this.disabled) {
            this.disableScroll('y');
        }
        else {
            this.enableScroll('y');
        }
    };
    DragScrollDirective.prototype.ngOnInit = function () {
        this.displayType = window.getComputedStyle(this.el.nativeElement).display;
        this.el.nativeElement.style.display = this.displayType;
        this.markElDimension();
        this.renderer.setAttribute(this.el.nativeElement, 'drag-scroll', 'true');
        document.addEventListener('dragstart', function (e) {
            e.preventDefault();
        });
    };
    DragScrollDirective.prototype.ngDoCheck = function () {
        this.childrenArr = this.el.nativeElement.children || [];
        if (this.childrenArr.length !== this.prevChildrenLength) {
            if (this.wrapper) {
                this.checkScrollbar();
            }
            this.prevChildrenLength = this.childrenArr.length;
            this.checkNavStatus();
        }
    };
    DragScrollDirective.prototype.ngOnDestroy = function () {
        this.renderer.setAttribute(this.el.nativeElement, 'drag-scroll', 'false');
        this.mouseMoveListener();
        this.mouseUpListener();
    };
    DragScrollDirective.prototype.onMouseMove = function (e) {
        if (this.isPressed && !this.disabled) {
            e.preventDefault();
            if (!this.xDisabled && !this.dragDisabled) {
                this.el.nativeElement.scrollLeft =
                    this.el.nativeElement.scrollLeft - e.clientX + this.downX;
                this.downX = e.clientX;
            }
            if (!this.yDisabled && !this.dragDisabled) {
                this.el.nativeElement.scrollTop =
                    this.el.nativeElement.scrollTop - e.clientY + this.downY;
                this.downY = e.clientY;
            }
        }
        return !this.isPressed;
    };
    DragScrollDirective.prototype.onMouseDown = function (e) {
        this.isPressed = true;
        this.downX = e.clientX;
        this.downY = e.clientY;
        clearTimeout(this.scrollToTimer);
    };
    DragScrollDirective.prototype.onScroll = function () {
        var _this = this;
        var ele = this.el.nativeElement;
        if ((ele.scrollLeft + ele.offsetWidth) >= ele.scrollWidth) {
            this.scrollReachesRightEnd = true;
        }
        else {
            this.scrollReachesRightEnd = false;
        }
        this.checkNavStatus();
        if (!this.isPressed && !this.isAnimating && !this.snapDisabled) {
            this.isScrolling = true;
            clearTimeout(this.scrollTimer);
            this.scrollTimer = window.setTimeout(function () {
                _this.isScrolling = false;
                _this.locateCurrentIndex(true);
            }, 500);
        }
        else {
            this.locateCurrentIndex();
        }
    };
    DragScrollDirective.prototype.onMouseUp = function (e) {
        if (this.isPressed) {
            this.isPressed = false;
            if (!this.snapDisabled) {
                this.locateCurrentIndex(true);
            }
            else {
                this.locateCurrentIndex();
            }
        }
    };
    DragScrollDirective.prototype.moveLeft = function () {
        var ele = this.el.nativeElement;
        if (this.currIndex !== 0 || this.snapDisabled) {
            this.currIndex--;
            clearTimeout(this.scrollToTimer);
            this.scrollTo(ele, this.toChildrenLocation(), 500);
        }
    };
    DragScrollDirective.prototype.moveRight = function () {
        var ele = this.el.nativeElement;
        if (!this.scrollReachesRightEnd && this.childrenArr[this.currIndex + 1]) {
            this.currIndex++;
            clearTimeout(this.scrollToTimer);
            this.scrollTo(ele, this.toChildrenLocation(), 500);
        }
    };
    DragScrollDirective.prototype.moveTo = function (index) {
        var ele = this.el.nativeElement;
        if (index >= 0 && index !== this.currIndex && this.childrenArr[index]) {
            this.currIndex = index;
            clearTimeout(this.scrollToTimer);
            this.scrollTo(ele, this.toChildrenLocation(), 500);
        }
    };
    DragScrollDirective.prototype.checkNavStatus = function () {
        var ele = this.el.nativeElement;
        var childrenWidth = 0;
        for (var i = 0; i < ele.children.length; i++) {
            childrenWidth += ele.children[i].clientWidth;
        }
        if (this.childrenArr.length <= 1 || ele.scrollWidth <= ele.clientWidth) {
            this.reachesLeftBound.emit(true);
            this.reachesRightBound.emit(true);
        }
        else if (this.scrollReachesRightEnd) {
            this.reachesLeftBound.emit(false);
            this.reachesRightBound.emit(true);
        }
        else if (ele.scrollLeft === 0 &&
            ele.scrollWidth > ele.clientWidth) {
            this.reachesLeftBound.emit(true);
            this.reachesRightBound.emit(false);
        }
        else {
            this.reachesLeftBound.emit(false);
            this.reachesRightBound.emit(false);
        }
    };
    DragScrollDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[dragScroll]'
                },] },
    ];
    DragScrollDirective.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
        { type: core_1.Renderer2, },
    ]; };
    DragScrollDirective.propDecorators = {
        "reachesLeftBound": [{ type: core_1.Output },],
        "reachesRightBound": [{ type: core_1.Output },],
        "scrollbarHidden": [{ type: core_1.Input, args: ['scrollbar-hidden',] },],
        "disabled": [{ type: core_1.Input, args: ['drag-scroll-disabled',] },],
        "xDisabled": [{ type: core_1.Input, args: ['drag-scroll-x-disabled',] },],
        "yDisabled": [{ type: core_1.Input, args: ['drag-scroll-y-disabled',] },],
        "dragDisabled": [{ type: core_1.Input, args: ['drag-disabled',] },],
        "snapDisabled": [{ type: core_1.Input, args: ['snap-disabled',] },],
        "onResize": [{ type: core_1.HostListener, args: ['window:resize', ['$event'],] },],
    };
    return DragScrollDirective;
}());
exports.DragScrollDirective = DragScrollDirective;
function DragScrollDirective_tsickle_Closure_declarations() {
    DragScrollDirective.decorators;
    DragScrollDirective.ctorParameters;
    DragScrollDirective.propDecorators;
    DragScrollDirective.prototype._scrollbarHidden;
    DragScrollDirective.prototype._disabled;
    DragScrollDirective.prototype._xDisabled;
    DragScrollDirective.prototype._yDisabled;
    DragScrollDirective.prototype._dragDisabled;
    DragScrollDirective.prototype._snapDisabled;
    DragScrollDirective.prototype.isPressed;
    DragScrollDirective.prototype.isScrolling;
    DragScrollDirective.prototype.scrollTimer;
    DragScrollDirective.prototype.scrollToTimer;
    DragScrollDirective.prototype.downX;
    DragScrollDirective.prototype.downY;
    DragScrollDirective.prototype.displayType;
    DragScrollDirective.prototype.elWidth;
    DragScrollDirective.prototype.elHeight;
    DragScrollDirective.prototype.parentNode;
    DragScrollDirective.prototype.wrapper;
    DragScrollDirective.prototype.scrollbarWidth;
    DragScrollDirective.prototype.onMouseMoveHandler;
    DragScrollDirective.prototype.onMouseDownHandler;
    DragScrollDirective.prototype.onScrollHandler;
    DragScrollDirective.prototype.onMouseUpHandler;
    DragScrollDirective.prototype.mouseMoveListener;
    DragScrollDirective.prototype.mouseDownListener;
    DragScrollDirective.prototype.scrollListener;
    DragScrollDirective.prototype.mouseUpListener;
    DragScrollDirective.prototype.currIndex;
    DragScrollDirective.prototype.isAnimating;
    DragScrollDirective.prototype.scrollReachesRightEnd;
    DragScrollDirective.prototype.prevChildrenLength;
    DragScrollDirective.prototype.childrenArr;
    DragScrollDirective.prototype.reachesLeftBound;
    DragScrollDirective.prototype.reachesRightBound;
    DragScrollDirective.prototype.el;
    DragScrollDirective.prototype.renderer;
}
var DragScrollModule = (function () {
    function DragScrollModule() {
    }
    DragScrollModule.decorators = [
        { type: core_1.NgModule, args: [{
                    exports: [DragScrollDirective],
                    declarations: [DragScrollDirective]
                },] },
    ];
    DragScrollModule.ctorParameters = function () { return []; };
    return DragScrollModule;
}());
exports.DragScrollModule = DragScrollModule;
function DragScrollModule_tsickle_Closure_declarations() {
    DragScrollModule.decorators;
    DragScrollModule.ctorParameters;
}
//# sourceMappingURL=ngx-drag-scroll.js.map