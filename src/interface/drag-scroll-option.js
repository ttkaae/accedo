"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function DragScrollOption() { }
exports.DragScrollOption = DragScrollOption;
function DragScrollOption_tsickle_Closure_declarations() {
    DragScrollOption.prototype.disabled;
    DragScrollOption.prototype.scrollbarHidden;
    DragScrollOption.prototype.yDisabled;
    DragScrollOption.prototype.xDisabled;
    DragScrollOption.prototype.nav;
}
//# sourceMappingURL=drag-scroll-option.js.map